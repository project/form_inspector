<?php

namespace Drupal\form_inspector;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\Random;
use Drupal\contextual\Element\ContextualLinks;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\RouteProviderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Form Inspector Manager.
 */
class FormInspectorManager implements FormInspectorManagerInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a FormInspectorManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger instance.
   */
  public function __construct(ModuleHandlerInterface $module_handler, RouteProviderInterface $route_provider, LoggerInterface $logger) {
    $this->moduleHandler = $module_handler;
    $this->routeProvider = $route_provider;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldWidgetFormAlter(array &$element, FormStateInterface &$form_state, array $context, $add_ancestors = TRUE, $add_wrapper = TRUE) {
    if (!empty($context['form']['#form_inspector_ancestors'])) {
      if ($add_ancestors) {
        $ancestors = implode('-', $context['form']['#form_inspector_ancestors']);
        $prefix = $element['#prefix'] ?? '';
        $element['#prefix'] = Markup::create("\n<!-- ANCESTORS {$ancestors} -->\n" . ($prefix instanceof MarkupInterface ? $prefix->__toString() : $prefix));
      }
      if ($this->moduleHandler->moduleExists('contextual') &&
        isset($context['items']) &&
        ($field_definition = $context['items']->getFieldDefinition()) &&
        method_exists($field_definition, 'id') &&
        $field = $field_definition->id()) {
        $ancestor = end($context['form']['#form_inspector_ancestors']);
        [$entity_type_id, $bundle] = explode('.', $ancestor);
        try {
          $route = $this->routeProvider->getRouteByName("entity.field_config.{$entity_type_id}_field_edit_form");
          $options = $route->getOptions();
          if (isset($options['parameters']) &&
            is_array($options['parameters']) &&
            ($keys = array_keys($options['parameters'])) &&
            $parameter = array_shift($keys)) {
            $random = new Random();
            $contextual_links = [
              $ancestor => [
                'route_parameters' => [
                  $parameter => $bundle,
                  'field_config' => $field,
                  // Makes data-contextual-id unique.
                  'form_inspector_rand' => $random->name(10, TRUE),
                ],
              ],
            ];
            if (isset($element['target_id'])) {
              $element['target_id']['#contextual_links'] = $contextual_links;
            }
            else {
              $element['#contextual_links'] = $contextual_links;
            }
            if ($entity_type_id == 'paragraph') {
              $element['contextual_links'] = [
                '#type' => 'contextual_links_placeholder',
                '#id' => _contextual_links_to_id($contextual_links),
              ];
              $element['#pre_render'] = [
                [
                  ContextualLinks::class,
                  'preRenderLinks',
                ],
              ];
              if ($add_wrapper) {
                $prefix = $element['#prefix'] ?? '';
                $element['#prefix'] = Markup::create(($prefix instanceof MarkupInterface ? $prefix->__toString() : $prefix) . '<div class="form-inspector-contextual-region contextual-region">');
                $suffix = $element['#suffix'] ?? '';
                $element['#suffix'] = Markup::create('</div>' . ($suffix instanceof MarkupInterface ? $suffix->__toString() : $suffix));
              }
            }
          }
        }
        catch (RouteNotFoundException $e) {
          $this->logger->warning($e->getMessage());
        }
      }
    }
  }

}
