<?php

namespace Drupal\form_inspector\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic contextual links for form inspector.
 */
class FormInspectorContextualLinks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * Constructs a new FormInspectorContextualLinks.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   Bundle info service.
   */
  public function __construct(EntityTypeBundleInfoInterface $bundle_info) {
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->bundleInfo->getAllBundleInfo() as $entity_type_id => $bundles) {
      $route_name = "entity.field_config.{$entity_type_id}_field_edit_form";
      if (\Drupal::service('router.no_access_checks')
        ->getRouteCollection()
        ->get($route_name)) {
        foreach ($bundles as $bundle => $label) {
          $derivative_id = "{$entity_type_id}.{$bundle}";
          $this->derivatives[$derivative_id] = $base_plugin_definition;
          $this->derivatives[$derivative_id]['class'] = '\Drupal\Core\Menu\ContextualLinkDefault';
          $this->derivatives[$derivative_id]['route_name'] = $route_name;
          $this->derivatives[$derivative_id]['group'] = $derivative_id;
          $this->derivatives[$derivative_id]['title'] = 'Edit field in ' . ($label['label'] ?? 'entity');
        }
      }
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
