<?php

namespace Drupal\form_inspector\Entity;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration entity that adds form element ancestors.
 */
class FormInspectorEntityFormDisplay extends EntityFormDisplay {

  /**
   * {@inheritdoc}
   */
  public function buildForm(FieldableEntityInterface $entity, array &$form, FormStateInterface $form_state) {
    $ancestors = &drupal_static(__METHOD__, []);
    $ancestor = $entity->getEntityTypeId() . '.' . $entity->bundle();
    if (!in_array($ancestor, $ancestors)) {
      $ancestors[] = $ancestor;
    }
    $form['#form_inspector_ancestors'] = $ancestors;
    parent::buildForm($entity, $form, $form_state);
  }

}
